// Escreva uma função que converta a data de entrada do usuário formatada como MM/DD/YYYY em um formato exigido por uma API (YYYYMMDD). O parâmetro "userDate" e o valor de retorno são strings.

// Por exemplo, ele deve converter a data de entrada do usuário "31/12/2014" em "20141231" adequada para a API.


function formatDate(userDate) {
  // format from M/D/YYYY to YYYYMMDD

  // convert parameter to a date
  var returnDate = new Date(userDate);

  // get the day, month and year
  var y = returnDate.getFullYear();
  var m = returnDate.getMonth() + 1;
  var d = returnDate.getDate();

  // converting the integer values we got above to strings
  y = y.toString();
  m = m.toString();
  d = d.toString();

  // making days or months always 2 digits
  if (m.length === 1) {
    m = '0' + m;
  }
  if (d.length === 1) {
    d = '0' + d;
  }

  // Combine the 3 strings together
  returnDate = y + m + d;

  return returnDate;
}

console.log(formatDate("12/31/2014"));