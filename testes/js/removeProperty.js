// Implemente a função removeProperty, que recebe um objeto e o nome de uma propriedade.

// Faça o seguinte:

// Se o objeto obj tiver uma propriedade prop, a função removerá a propriedade do objeto e retornará true;
// em todos os outros casos, retorna falso.

function removeProperty(obj, prop) {
  Object.keys(obj).reduce((object, key) => {
    if (key === prop) {
      return delete object[key]
    }else{
      return false;
    }
  }, {})
}

const user = {
  name: 'Aristoteles Lopes',
  age: '25'
} 

removeProperty(user, 'name')