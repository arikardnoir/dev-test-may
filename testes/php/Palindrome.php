<?php

/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*
Um palíndromo é uma palavra que pode ser lida  da mesma maneira da esquerda para direita, como ao contrário.
Exemplo: ASA.

Faça uma função que ao receber um input de uma palavra, string, verifica se é um palíndromo retornando verdadeiro ou falso.
O fato de um caracter ser maiúsculo ou mínusculo não deverá influenciar no resultado da função.

Exemplo: isPalindrome("Asa") deve retonar true.
*/

class Palindrome
{
    //In this function i did it manually
    public static function isPalindromeManually($word)
    {
        $aux  = strtoupper($word);
        $size = strlen($word);
        $count  = 0;
        
        for ($i=0; $i < $size; $i++) { 
            if (strtoupper($word[$i]) == $aux[$size - ($i + 1)]) {
                $count++;
            }
        }

        if ($size == $count) {
            return true;
        }else{
            return false;
        }
    }

    //In this function i did it with strrev function help
    public static function isPalindrome($word)
    {
        if (strtoupper($word) == strrev(strtoupper($word)))
            return true;
        else
            return false;
    }
}

echo Palindrome::isPalindrome('Deleveled');