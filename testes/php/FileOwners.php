<?php


/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*
Implemente uma função que ao receber um array associativo contendo arquivos e donos, retorne os arquivos agrupados por dono. 

Por exempolo, um array ["Input.txt" => "Jose", "Code.py" => "Joao", "Output.txt" => "Jose"] a função groupByOwners deveria retornar ["Jose" => ["Input.txt", "Output.txt"], "Joao" => ["Code.py"]].


*/

class FileOwners
{

    public static function groupByOwners($files)
    {
        $files_return = [];
                
        foreach($files as $x => $x_value) {
            if (array_key_exists($x_value,$files_return))
                array_push($files_return[$x_value], $x);
            else {
                $files_return[$x_value] = array();
                array_push($files_return[$x_value], $x);
            }
        }
        
        return $files_return;
    }
}

$files = array
(
    "Input.txt" => "Jose",
    "Code.py" => "Joao",
    "Output.txt" => "Jose",
    "Click.js" => "Maria",
    "Out.php" => "maria",
);
var_dump(FileOwners::groupByOwners($files));
